# constants dont change (things like 100, 89.3)
# string constants also don't change ("hello world" will always be "hello world")
#
# reserved words cannot be used for anything but its intended purpose (no variables called "if" or "while")
#
# variable - python will allocate memory to create a value with a name that you assign
# x = 10 will create a memory chunk called x with a value of 10
# x = 11 will just overwrite the value
# mnemonic variable names help with reading code
#
# + addition        - subtration        * multiplication        / division      ** power        % modulo (returns remainder)
# 
# PEMDAS still works
# 
# knows difference between types of data
#
# traceback error: python can't continue do to a certain line
# 
# type() function returns the type of the argument, type(1) would return int
# float(), int(), str() can be used to convert ints and floats to different types
#
# floats have greater range than int but less precision
# 
# integer division will always return to floats in python 3, not the case in python 2
# 
# number strings can be used as ints or floats using int() or float()
# x = "123"
# print(int(123) + 1) returns 124
# 
# letter strings will not work this way
# 
# input() can be used to prompt for user input
# returns a string 
 
x = input("Who are you?")
print("Welcome",x)

# returns "Welcome, x"
#  