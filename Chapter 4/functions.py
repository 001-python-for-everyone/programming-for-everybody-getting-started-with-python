# functions use the following format
# def name(args):
#   code block
# 
# not executed until called
# like a variable but executes code
# def does not run the function code block
# 
# args are not variables but placeholders
# 
# return does 2 things
# ends the function block
# remembers a value to output 
#  