largest = None
smallest = None
while True:
    num = input("Enter a number: ")
    if num == "done" : break
    try:
        num = int(num)
    except:
        print("Thats not a valid input")
        continue
    if(largest == None):
        largest = num
    if(smallest == None):
        smallest = num
    if(num > largest): largest = num
    if(num < smallest): smallest = num
    print(num)

print("Maximum", largest)
print("Minimum", smallest)
